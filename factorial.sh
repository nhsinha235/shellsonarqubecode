 num=$1

local fact=1

for((i=2;i<=num;i++))
{
  fact=$((fact * i))  #fact = fact * i
}

echo "factorial of $1 is $fact"
